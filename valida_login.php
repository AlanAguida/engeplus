<?php
	session_start();
	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	require_once "autoload.php";

	$Usuario = new Usuario();
	$UsuarioDAO = new UsuarioDAO();

	$Usuario->setUsuario($_POST['usuario']);
	$Usuario->setSenha($_POST['senha']);

	$UsuarioPassou = $UsuarioDAO->logaUsuario($Usuario);


	if(empty($UsuarioPassou)){

		header("LOCATION: login.php");
		$_SESSION['danger'] = 'true';

	}else{

		$_SESSION['usuarioIdAdmin'] = $UsuarioPassou[0]['id'];
		$_SESSION['usuarioNomeAdmin'] = $Usuario->getUsuario();

		setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
		date_default_timezone_set('America/Sao_Paulo');

		$ip=$_SERVER['REMOTE_ADDR'];
		$datahora=date("Y-m-d H:i:s");

		$UsuarioDAO->cadatraAcesso($UsuarioPassou[0]['id'],$ip,$datahora);

		header("LOCATION: index.php");

	}
?>