<?php
	class Pedido{

	

		public $tabela ="en_pedido";

    	//gera atributos para classe
    	public function setAtributos($arrAttr){
    		foreach ($arrAttr as $attrAtual) {
				$this->{$attrAtual['Field']}="";
			}
    	}

		public function __call($method, $args) {
			// gera os sets
	      	foreach (get_object_vars($this) as $key => $value) {
	      		if ($method == 'set'.ucfirst($key).'') {
	      		 	$this->$key = $args[0];
	      		}
	      	}

	      	// gera os gets
	      	foreach (get_object_vars($this) as $key => $value) {
	      		if ($method == 'get'.ucfirst($key).'') {
	      			return $this->$key;
	      		}
	      	}
    	}	

	}
?>