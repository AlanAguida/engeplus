<?php
	class UsuarioDAO{

		private $conexao;

		function __construct(){
			$this->conexao = DB::conexao();
		}

		public function logaUsuario(Usuario $Usuario){
			$stmt = $this->conexao->prepare("SELECT * FROM en_login  where nome = ? and senha = ? limit 1 ");
			$stmt->bindValue(1, $Usuario->getUsuario(),PDO::PARAM_STR);
			$stmt->bindValue(2, $Usuario->getSenhaCriptografada(),PDO::PARAM_STR);
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $result;

		}

		public function cadatraAcesso($id_usuario="", $ip="", $datahora=""){
			$stmt = $this->conexao->prepare("INSERT INTO en_acessos (id_usuario, ip, datahora) VALUES(?,?,?)");
			$stmt->bindValue(1, $id_usuario, PDO::PARAM_STR);
			$stmt->bindValue(2, $ip, PDO::PARAM_STR);
			$stmt->bindValue(3, $datahora, PDO::PARAM_STR);
			$stmt->execute();
		}

		public function usuarioEstaLogado(){
			return isset($_SESSION['usuarioIdAdmin']);
		}

		public function usuarioLogado(){
			return $_SESSION['usuarioNomeAdmin'];
		}

		public function buscaUsuario($id){

			if(!empty($id)){
				$query = $this->conexao->prepare("SELECT * FROM en_login WHERE id=:id");
				$query->bindParam(':id', $id, PDO::PARAM_INT);
				$query->execute();
				$query = $query->fetch(PDO::FETCH_ASSOC);

				return $query;
			}else{
				header("LOCATION: model/sair.php");
				exit;
			}

		}

		public function buscaAcessos($id){

			if(!empty($id)){
				$stmt = $this->conexao->prepare("SELECT * FROM en_acessos  where id_usuario = ?  order by datahora desc limit 1");
				$stmt->bindValue(1, $id,PDO::PARAM_STR);
				$stmt->execute();
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				return $result;
			}else{
				header("LOCATION: model/sair.php");
				exit;
			}

		}

		public function deslogaUsuario(){

			unset($_SESSION['usuarioIdAdmin']);
			unset($_SESSION['usuarioNomeAdmin']);
			//session_start();

			header("LOCATION: ../login.php");

			die();

		}


	}
?>