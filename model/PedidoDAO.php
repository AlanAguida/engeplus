<?php
	class PedidoDAO{

		private $conexao;

		function __construct(){
			$this->conexao = DB::conexao();
		}

		//listagem no sistema interno (não alterar)
		public function listaPedidos(){
			/*$stmt = $this->conexao->prepare("SELECT * FROM cd_produtos INNER JOIN cd_categoria_produtos on cd_produtos.categoria_pdt = cd_categoria_produtos.id_pdt_ctg
				ORDER BY cd_produtos.id_pdt DESC
			");*/

			/*$stmt = $this->conexao->prepare("SELECT * FROM cd_produtos INNER JOIN cd_categoria_produtos on cd_categoria_produtos.id_pdt_ctg = cd_produtos.categoria_pdt
				ORDER BY cd_produtos.id_pdt DESC
			");*/

			$stmt = $this->conexao->prepare("SELECT p.id_pedido,ven.nome_vendedor, c.nome_cliente,
              SUM(vi.comissao_item) AS comissao FROM en_pedido p INNER JOIN en_item_pedido vi ON vi.ped_id = p.id_pedido INNER JOIN en_cliente c ON c.id_cliente = p.id_cliente INNER JOIN en_vendedor ven ON ven.id_vendedor=p.id_cliente GROUP BY p.id_pedido ORDER BY p.id_pedido DESC");
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$todosPedidos = array();

			foreach ($result as $rs) {

				$Pedidos = new Pedido();
				$Mobject = new Mobject();
			    $Mobject->tabela($Pedidos->tabela); //tabela do banco
			    $Pedidos->setAtributos($Mobject->atributos());//seta atributos no objeto
			    $Mobject->setReturn($Pedidos,$rs); //Monta objeto com os dados do banco

				array_push($todosPedidos, $Pedidos);
			}
			return $todosPedidos;
		}

		public function buscaPedido($id){

			if(!empty($id)){
				$query = $this->conexao->prepare("SELECT * FROM en_pedido  WHERE id_pedido=$id");			
				$query->execute();
				$query = $query->fetch(PDO::FETCH_ASSOC);			
				return $query;
			}else{
				header("LOCATION: model/sair.php");
				exit;
			}

		}

		


	}
?>