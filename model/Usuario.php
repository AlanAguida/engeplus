<?php
	class Usuario{

		public $id;
		public $nome_apresentacao;
		public $usuario;
		public $senha;
		public $ip;
		public $datahora;

		public function getId(){ return $this->id; }
		public function getNomeApresentacao(){ return $this->usuario; }
		public function getUsuario(){ return $this->usuario; }
		public function getSenha(){ return $this->senha; }

		public function setId($id){ $this->id = $this->ehInteiro($id); }
		public function setNomeApresentacao($nome){ $this->nome_apresentacao = $this->antiInjection($nome); }
		public function setUsuario($usuario){ $this->usuario = $this->antiInjection($usuario); }
		public function setSenha($senha){ $this->senha = $this->antiInjection($senha); }

		public function getSenhaCriptografada(){
			if(!empty($this->senha)) return hash('whirlpool', $this->senha);
		}

		public function ehInteiro($inteiro){
			if(is_numeric($inteiro)) return $inteiro;
		}

		public function antiInjection($string){
			$string = addslashes(strip_tags($string));
			return $string;
		}

	}
?>