<?php
	class VendedorDAO{

		private $conexao;

		function __construct(){
			$this->conexao = DB::conexao();
		}

		//listagem no sistema interno (não alterar)
		public function listaVendedor(){
			/*$stmt = $this->conexao->prepare("SELECT * FROM cd_produtos INNER JOIN cd_categoria_produtos on cd_produtos.categoria_pdt = cd_categoria_produtos.id_pdt_ctg
				ORDER BY cd_produtos.id_pdt DESC
			");*/

			/*$stmt = $this->conexao->prepare("SELECT * FROM cd_produtos INNER JOIN cd_categoria_produtos on cd_categoria_produtos.id_pdt_ctg = cd_produtos.categoria_pdt
				ORDER BY cd_produtos.id_pdt DESC
			");*/

			$stmt = $this->conexao->prepare("SELECT * FROM en_vendedor)");
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$todosVendedores = array();

			foreach ($result as $rs) {

				$Vendedores = new Vendedor();
				$Mobject = new Mobject();
			    $Mobject->tabela($Vendedores->tabela); //tabela do banco
			    $Vendedores->setAtributos($Mobject->atributos());//seta atributos no objeto
			    $Mobject->setReturn($Vendedores,$rs); //Monta objeto com os dados do banco

				array_push($todosVendedores, $Vendedores);
			}
			return $todosVendedores;
		}

		public function buscaVendedor($id){

			if(!empty($id)){
				$query = $this->conexao->prepare("SELECT * FROM en_vendedor  WHERE id_vendedor=$id");			
				$query->execute();
				$query = $query->fetch(PDO::FETCH_ASSOC);			
				return $query;
			}else{
				header("LOCATION: model/sair.php");
				exit;
			}

		}

		


	}
?>