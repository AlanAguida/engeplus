<?php
	class ClienteDAO{

		private $conexao;

		function __construct(){
			$this->conexao = DB::conexao();
		}

		//listagem no sistema interno (não alterar)
		public function listaClientes(){
			/*$stmt = $this->conexao->prepare("SELECT * FROM cd_produtos INNER JOIN cd_categoria_produtos on cd_produtos.categoria_pdt = cd_categoria_produtos.id_pdt_ctg
				ORDER BY cd_produtos.id_pdt DESC
			");*/

			/*$stmt = $this->conexao->prepare("SELECT * FROM cd_produtos INNER JOIN cd_categoria_produtos on cd_categoria_produtos.id_pdt_ctg = cd_produtos.categoria_pdt
				ORDER BY cd_produtos.id_pdt DESC
			");*/

			$stmt = $this->conexao->prepare("SELECT * FROM en_cliente");
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$todosClientes = array();

			foreach ($result as $rs) {

				$Clientes = new Cliente();
				$Mobject = new Mobject();
			    $Mobject->tabela($Clientes->tabela); //tabela do banco
			    $Clientes->setAtributos($Mobject->atributos());//seta atributos no objeto
			    $Mobject->setReturn($Clientes,$rs); //Monta objeto com os dados do banco

				array_push($todosClientes, $Clientes);
			}
			return $todosClientes;
		}

		public function buscaClientes($id){

			if(!empty($id)){
				$query = $this->conexao->prepare("SELECT * FROM en_cliente  WHERE id_cliente=$id");				
				$query->execute();
				$query = $query->fetch(PDO::FETCH_ASSOC);
				return $query;
			}else{
				header("LOCATION: model/sair.php");
				exit;
			}

		}

		


	}
?>