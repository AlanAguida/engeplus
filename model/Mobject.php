<?php
	error_reporting(E_ALL);

	class Mobject{

		private $conexao;
		public $camposlista;

		function __construct(){
			$this->conexao = DB::conexao();
		}

		public function tabela($tabela){
			$camposlista= array();

			$stmt = $this->conexao->prepare("SHOW COLUMNS FROM $tabela ");
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach ($result as $campos) {
				array_push($camposlista, $campos);
			}

			$this->camposlista = $camposlista;
		}

		public function atributos(){
			return $this->camposlista;
		}

		//seta retorno de dados
		public function setReturn($objeto,$result){

			foreach ($result as $key => $valor) {
				$objeto->{$key} =$valor; //cria atributos conforme o retorno do banco
			}
		}
	}
?>