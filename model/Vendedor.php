<?php
	class Vendedor{

	

		public $tabela ="en_vendedor";

    	//gera atributos para classe
    	public function setAtributos($arrAttr){
    		foreach ($arrAttr as $attrAtual) {
				$this->{$attrAtual['Field']}="";
			}
    	}

		public function __call($method, $args) {
			// gera os sets
	      	foreach (get_object_vars($this) as $key => $value) {
	      		if ($method == 'set'.ucfirst($key).'') {
	      		 	$this->$key = $args[0];
	      		}
	      	}

	      	// gera os gets
	      	foreach (get_object_vars($this) as $key => $value) {
	      		if ($method == 'get'.ucfirst($key).'') {
	      			return $this->$key;
	      		}
	      	}
    	}
	

		public function getId(){ return $this->id; }
		public function getNome(){ return $this->usuario; }
		//public function getSenha(){ return $this->email; }

		public function setId($id){ $this->id = $this->ehInteiro($id); }
		public function setNome($nome){ $this->nome = $this->antiInjection($nome); }
		public function setUsuario($usuario){ $this->usuario = $this->antiInjection($usuario); }
		//public function setSenha($senha){ $this->senha = $this->antiInjection($senha); }
		public function setSenha($email){ $this->email = $this->antiInjection($email); }

		public function getSenhaCriptografada(){
			if(!empty($this->senha)) return hash('whirlpool', $this->senha);
		}

		public function ehInteiro($inteiro){
			if(is_numeric($inteiro)) return $inteiro;
		}

		public function antiInjection($string){
			$string = addslashes(strip_tags($string));
			return $string;
		}

	}
?>