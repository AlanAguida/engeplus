<?php
	/**
	* Funções úteis
	*/
	setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');

	class Util{

		public static function dataHoraAtual(){
			$dataAtual = new DateTime();
			$dataFinal = $dataAtual->format('Y-m-d H:i:s');
			return $dataFinal;
		}

		public static function dataAtual(){
			$dataAtual = new DateTime();
			$dataFinal = $dataAtual->format('Y-m-d');
			return $dataFinal;
		}

		public function dataHoraDecode($valor){
			$d = new DateTime($valor);
			if (!empty($valor)) {
				# code...
				$datafinal = $d->format('d/m/Y H:i'); // 2011-01-01T15:03:01.012345
			}else{
				$datafinal ="";
			}
			return $datafinal;
		}

		public function dataHoraEncode($valor){
			if (!empty($valor)) {//verifica se a dada é zerada e manda a data zerada
				$valor=str_replace("/", "-",$valor);
				$d = new DateTime($valor);
				$datafinal = $d->format('Y-m-d H:i:00'); // 2011-01-01T15:03:01.012345
				return $datafinal;
			}else{
				return $valor;
			}
		}


		public function dataDecode($valor){
			$d = new DateTime($valor);
			$datafinal = $d->format('d/m/Y'); // 2011-01-01T15:03:01.012345
			return $datafinal;
		}


		public function dataEncode($valor){
			if (!empty($valor)) {//verifica se a dada é zerada e manda a data zerada
				$valor=str_replace("/", "-",$valor);
				$d = new DateTime($valor);
				$datafinal = $d->format('Y-m-d'); // 2011-01-01T15:03:01.012345
				return $datafinal;
			}else{
				return $valor;
			}
		}

		function dataExtenso($data){

			$d = new DateTime($data);
			$data = $d->format('Y-m-d'); // 2011-01-01T15:03:01.012345

            $datafinal = strftime("%d de ",strtotime($data));
            $datafinal .= ucfirst(strftime("%b",strtotime($data)));// primeira maiúscula
            $datafinal .= strftime(" de %Y",strtotime($data));
            return $datafinal;
		}

		//muda as tags html
		function dataExtenso2($data){

			$d = new DateTime($data);

			setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');
			$data = $d->format('Y-m-d'); // 2011-01-01T15:03:01.012345

            $datafinal = strftime(" <div>%d <span> %b</span></div> ",strtotime($data));
            return $datafinal;
		}

		function dataHoraExtenso($data){

			$d = new DateTime($data);

			setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');
			$data = $d->format('Y-m-d h:i:s A T'); // 2011-01-01T15:03:01.012345

            //$datafinal = strftime(" <h4>%d</h4>  <h5>%b.</h5> ",strtotime($data));
            $datafinal = strftime("  %b, %d , %Y at %H:%M %P ",strtotime($data));

            return $datafinal;
		}

		//formata preço para ir ao banco
		function  precoBanco($valor){
			$replaces = array("R", "$", ":"," ");
			$valor = str_replace($replaces, "", $valor);
		 	//$valor = str_replace("R$:", "", $valor);
		 	$valor = str_replace(".", "", $valor);
		 	$valor = str_replace(",", ".", $valor);
		 	return $valor;
		}

		function resume( $var, $limite){	// Se o texto for maior que o limite, ele corta o texto e adiciona 3 pontinhos.
			if (strlen($var) > $limite)	{
				$var = substr($var, 0, $limite);
				$var = trim($var)."...</p>";
			}
			return $var;
		}

		//formata preço para ir na mascada
		function  precoMascara($valor){
			$valor = str_replace(".", ",", $valor);
			return $valor;
		}

		//formata preço para ir na mascada
		function  aNegociar($valor){
			if ($valor == "0,00") {
				$valor = "A negociar";
				return $valor;
			}else{

				return "R$ ".$valor;
			}

		}

		function randomString($size="4") {
			$codigoFinal="";
			$numeros =str_split('0123456789');//10caracteres

			for ($i=1; $i <=$size; $i++) {
				$saida="";
					$saida = $numeros[rand(0, 9)];


				//verifica e elimina caractere duplicado
				if (strpos($codigoFinal, $saida) !== false) {
				    $i--;
				}else{
					$codigoFinal.=$saida;
				}

			}
			return $codigoFinal;
		}

		function mergeCode($size="4",$id="") {
			$tamanho="";
			$codigoFinal="";
			//verifica se id é menor que size e id não é vazio
			if (strlen($id) > 0 && strlen($id) < $size) {
				$tamanho = ($size - strlen($id));//calcula quantos caracteres faltam para o tamanho do size
				$codigoFinal = $this->randomString($tamanho);
				$codigoFinal .=$id;
			}
			return $codigoFinal;
		}

	}
?>