<script src="../../js/fancybox/fancybox.js"></script>
<script src="../../js/grid.js"></script>
<script src="../../js/maskedPlugin.js"></script>
<script src="../../js/maskmoney/src/jquery.maskMoney.js"></script>

<script src="../../js/ui/jquery.ui.core.js"></script>
<script src="../../js/ui/jquery.ui.widget.js"></script>
<script src="../../js/ui/jquery.ui.datepicker.js"></script>

<script src="../../js/tinymce/js/tinymce/tinymce.min.js"></script>

<!-- DATA TABLE -->
<script src="../../js/data_table/js/vendor/jquery.sortelements.js" type="text/javascript"></script>
<script src="../../js/data_table/js/jquery.bdt.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/datetimepicker/datetimepicker/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../js/datetimepicker/js/locales/bootstrap-datetimepicker.pt.js" charset="UTF-8"></script>

<script type="text/javascript">
jQuery(document).ready(function($) {
        $('.form_datetime').datetimepicker({
            language:  'pt',
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: false,
            format: 'dd/mm/yyyy hh:ii',

        });



    tinymce.init({
        language : "pt_BR",
        skin     : "lightgray",
        selector : "textarea",
        plugins: [
            "advlist autolink lists link charmap",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | bullist numlist | alignleft aligncenter alignright alignjustify"
     });


    $("#datepicker").datepicker();

    $('.fancy').fancybox({
        'showNavArrows' :   true
    });

    $('.fancy_iframe').fancybox({
        type        : 'iframe',
        maxWidth    : 300,
        maxHeight   : 200,
        fitToView   : false,
        autoSize    : true,
        closeClick  : true,
        openEffect  : 'none',
        closeEffect : 'none'
    });


    var displaySize =$(window).width();
    var ResDesktop = 1135;
    var ResTablet = 800;
    var ResCelular = 500;

    $(window).grid(displaySize,ResDesktop,ResTablet,ResCelular);

    $(window).stop().resize(function(event) {
        var displaySize =$(window).width();
        $(window).grid(displaySize,ResDesktop,ResTablet,ResCelular);
    });

    window.setTimeout(function() {
        var minWidth= 500; //resolução minima
        var element =".autoHeight";
        var WindowWidth = $(window).width();
        if(WindowWidth >= minWidth){
            $(element).autoHeight();
        }
        autoResize(element,minWidth);
    }, 2000);

    window.setTimeout(function() {
        var minWidth= 800; //resolução minima
        var element =".autoHeight2";
        var WindowWidth = $(window).width();
        if(WindowWidth >= minWidth){
            $(element).autoHeight();
        }
        autoResize(element,minWidth);
    }, 2000);


    window.setTimeout(function() {
        var minWidth= 500; //resolução minima
        var element =".autoHeight-footer";
        var WindowWidth = $(window).width();
        if(WindowWidth >= minWidth){
            $(element).autoHeight();
        }
        autoResize(element,minWidth);
    }, 2000);

    window.setTimeout(function() {
        var minWidth= 500; //resolução minima
        var element =".autoHeight-index";
        var WindowWidth = $(window).width();
        if(WindowWidth >= minWidth){
            $(element).autoHeight();
        }
        autoResize(element,minWidth);
    }, 2000);


   $(".mask-data").mask("99/99/9999");
   $(".mask-hora").mask("99:99");
   $(".mask-cep").mask("99999-999");
   $(".mask-telefoneUS").mask("(999) 999-9999");
   $(".mask-telefone").mask("(99) 9999-9999?9");
   $(".mask-cpf").mask("999.999.999-99");
   $(".mask-cnpj").mask("99999-999");
   $(".mask-text").mask("aaa-9999");
   $(".mask-processo").mask("99/9999");

   $(".mask-preco").maskMoney({
         prefix: "R$: ",
         decimal: ",",
         thousands: "."
     });

   $('.tabelaListagem').bdt({
        showSearchForm: 0,
        showEntriesPerPageField: 0
    });

   //desabilita envio post do campo de busca
   $("#search").attr('autocomplete', 'off');{

   }
   $("#search").parents("form").submit(function(e){
        return false;
    });

    jQuery(document).ready(function($) {
        $('.ajax-ordenacao').change(function(event) {
            valId = $(this).parent().children('[name="id"]').val();
            valOrdenacao = $(this).val();
            console.log(valId);
            if (valOrdenacao <0) {
                alert("O Mínimo é 0");
            }else{
                $.post("ordenacao.php", {ordenacao:valOrdenacao, id:valId}, function(result){
                    alert(result);
                });
            }

        });

        //seleciona valor no select
        function autoSelect(){
            $("select[data-value]").each(function(index, el) {
                var value = $(this).attr('data-value');
                $(this).children().each(function(index, e) {
                    if ($(this).val() == value) {
                        $(this).attr('selected', 'selected');
                    }
                });
            });
            callbackAutoSelect();
        }
        autoSelect();

    });
});

</script>

<!-- SELECT MULTIPLO-->
  <script>
    jQuery(document).ready(function($) {

      $(".select-multiple").hover(function() {
        $(this).find('ul').addClass('open');
      }, function() {
        $(this).find('ul').removeClass('open');
      });


      //marca os checkbox automaticamente
      $('.select-multiple>input').each(function(event) {
        var value = $(this).val().split(",");
        //console.log(value);
        $(this).parent().find('ul input').each(function(index, el) {
          if (value.indexOf($(this).val()) >=0) {
            $(this).attr('checked', 'checked');
          }
        });

      });

      function atualizaDados(){
        $('.select-multiple>input').each(function(event) {
          //array de valores
          var value = $(this).val().split(",");
          //console.log(value);

          $(this).parents('li').find('ul input').each(function(index, el) {
            //se encontra no array de values
            if (value.indexOf($(this).val()) >=0) {
              $(this).attr('checked', 'checked');
            }
          });
          //console.log(value);

          list = new Array();
          nomes = new Array();
          $(this).parent().find('input').each(function(index, el) {
            if ($(this).is(":checked")) {
              //console.log($(this).val());
              if ($(this).val()!="") {
                list.push($(this).val());
                nomes.push($(this).parent().find('p').html());
              }
              //console.log(nomes);
            }
          });

          $(this).parents('.select-multiple').find('.nome-selecionados').html(nomes.join());// nomes
          $(this).parents('.select-multiple').children('input').val(list.join());// values

          if (nomes.length == 0) {
            $(this).parents('.select-multiple').find('.nome-selecionados').text("Selecione");
          }
        });
      }

      atualizaDados();
      // select multiplo
      $('.select-multiple ul input').change(function(event) {
        atualizaDados();
      });

      /*auto-check*/
      var seletor = $("input[type='checkbox'][data-value]");
      autoCheck(seletor);

      $(document).on('DOMNodeInserted', "*", function (e) {
        var seletor = $("input[type='checkbox'][data-value]");
        autoCheck(seletor);
      });

      $('body').on('change', 'select[name="fk_acessorio_pdt"]', function(event) {
        verificaAcessorio('select[name="fk_acessorio_pdt"]');
      });

  });
  
  function callbackAutoSelect(){
    verificaAcessorio('select[name="fk_acessorio_pdt"]');
  }

  function verificaAcessorio(obj) {
    var value = $(obj).val();
    $('.dados-acessorios').hide(0);
    $('.dados-produto').hide(0);

    if (value == 1) {
      //console.log('é acessorios');
      $('.dados-acessorios').show(0);
    }else{
      $('.dados-produto').show(0);
      //console.log('não é acessório');
    }

  }


  function autoCheck(seletor){

    $(seletor).each(function(index, el) {
      var dataValue = $(this).attr('data-value');

      if (dataValue == 1) {
        $(this).attr('checked','checked');
      }else{
        $(this).removeAttr('checked');
      }
    });
  }
  </script>

    <script>
      $('#toggle').click(function () {
          $(this).toggleClass('active');
          $('#overlay').toggleClass('open');
      });

      $(".overlay ul li a").click(function () {
          $("#toggle").removeClass("active");
          $("#overlay").removeClass("open");
      })
  </script>
