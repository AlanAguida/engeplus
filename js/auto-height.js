(function($){
	 $.fn.autoHeight=function(){

		$(this).css('min-height', 'auto');
		elements= $(this);
		var tempAltura =0;
		$(elements).each(function(index, el) {
			$(elements).css('min-height', '0');
			$(elements).addClass('no__Transition');//precisa da classe css para  remover transições 
			var altura = $(this).outerHeight();
				//console.log(altura);
			if (altura>tempAltura) {
				tempAltura = altura;
				//console.log("maior"+tempAltura);
				$(elements).css('min-height', tempAltura+1); //+1 por que o a funcÃ£o autoHeight() arredonta pra baixo e quebra o grid
			}else{
				$(elements).css('min-height', tempAltura+1);
			}
			$(elements).removeClass('no__Transition');
		});
	}
})( jQuery );

//on resize chama "autoHeight"
function autoResize(element,minWidth) {
	$(window).resize(function(event) {
		$(element).css('min-height', 'auto');
		$(element).addClass('no__Transition'); //precisa da classe css para  remover transições 
        console.log(this);
		WindowWidth = $(window).width();
		if(WindowWidth >= minWidth){
			$(element).autoHeight();
		}else{
			$(element).css('min-height', 'auto');
		}
		$(element).removeClass('no__Transition');
	});
}
