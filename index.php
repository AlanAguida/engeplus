<?php
	session_start();
	require_once "autoload.php";
	$Usuario = new Usuario();
	$UsuarioDAO = new UsuarioDAO();


	$acessos = $UsuarioDAO->buscaAcessos($_SESSION['usuarioIdAdmin']);

	setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');
	$date = date_create($acessos['datahora']);
	$dataAcesso = date_format($date, 'd/m/Y H:i:s');
	$NomeUser = $_SESSION['usuarioNomeAdmin'];

	$usuarioDAO = new UsuarioDAO();

	if(!$usuarioDAO->usuarioEstaLogado()){
		$usuarioDAO->deslogaUsuario();
		exit;
	}
	$conexao = DB::conexao();	
	include_once "model/VendedorDAO.php";
	include_once "model/Vendedor.php";
	include_once "model/Mobject.php";
	include_once "model/Util.php";
	include_once "model/Cliente.php";
	include_once "model/ClienteDAO.php";
	$Util =  new Util();
	$clientes = new ClienteDAO();
	$cliente = new Cliente();
	$vendedor = new Vendedor();
	$vendedorDAO = new VendedorDAO();
	
	$cliente = $clientes->listaClientes();

	$id_vendedor = $_SESSION['usuarioIdAdmin'];
	$vendedor = $vendedorDAO->buscaVendedor($id_vendedor);
	$vendedor_logado = $vendedorDAO->buscaVendedor($id_vendedor);
	$acessos = $UsuarioDAO->buscaAcessos($_SESSION['usuarioIdAdmin']);
	setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');
	$date = date_create($acessos['datahora']);
	$dataAcesso = date_format($date, 'd/m/Y H:i:s');
	$NomeUser = $_SESSION['usuarioNomeAdmin'];
	
	$total = 0;
	
	$carrinho = (is_array($_SESSION['carrinho']) ? count($_SESSION['carrinho']) : 0);
	
	if($carrinho > 0):
    	foreach($_SESSION['carrinho'] as $idProd => $qtd){
    		$pegaProduto = $conexao->prepare("SELECT * FROM en_produtos WHERE id = ?");
    		$pegaProduto->execute(array($idProd));
    		$dadosProduto = $pegaProduto->fetchObject();
    		$subTotal = ($dadosProduto->valor*$qtd);
    		$total += $subTotal;
    	}
	endif;

	if(isset($_POST['finalizar']) && isset($_SESSION['carrinho'])){
	    $total = number_format(str_replace(",",".",str_replace(".",".",$total)), 2, '.', '');
			
			$dataAtual = $Util->dataAtual();
			$data_admissao = $Util->dataDecode($vendedor['data_admissao']);
			$diferenca = strtotime($dataAtual) - strtotime($data_admissao);
		    $dias = floor($diferenca / (60 * 60 * 24));
					
			$id_cliente = filter_var($_POST['id_cliente'], FILTER_VALIDATE_INT);

		   $inserePedido = $conexao->prepare("INSERT INTO en_pedido (id_cliente) VALUES ($id_cliente)");
		   $inserePedido->execute();
		   $pedido = $conexao->lastInsertId();					   

		   foreach($_SESSION['carrinho'] as $ProdInsert => $Qtd):
    		   	//var_dump($_SESSION['carrinho']);
    		   	$pegaValor = $conexao->prepare("SELECT valor, cat_prod_id from en_produtos INNER JOIN en_categoria_produto  WHERE en_produtos.id = $ProdInsert");
    
    		   	$pegaValor->execute();
    		   	$pegaValor = $pegaValor->fetchAll(PDO::FETCH_ASSOC);
    		   	foreach ($pegaValor as $valorProduto) {
    		   		
    		   		if($dias<1800){		   			   			
    		   			if($valorProduto['cat_prod_id'] == 1){				
    		   					$comissao_item = number_format($valorProduto['valor'] * 0.10,2,'.',',');
    		   			}if($valorProduto['cat_prod_id'] == 2){
    		   					$comissao_item = number_format($valorProduto['valor'] * 0.25,2,'.',',');
    		   			}		   		
    		   		}else{
    		   			$comissao_item = number_format($valorProduto['valor'] * 0.30,2,'.',',');  		
    		   		}
    		   			$totalItem = $valorProduto['valor'] * $Qtd;  
    		   			$totalItem = number_format(str_replace(",",".",str_replace(".",".",$valorProduto['valor'])), 2, '.', '');
    		   	}
    
    		  	$SqlInserirItens = $conexao->prepare("INSERT INTO en_item_pedido (item_id, prod_id,ped_id, quantidade, valor_item, comissao_item, id_vendedor, total_pedido) VALUES(null, '$ProdInsert','$pedido','$Qtd', '$totalItem', '$comissao_item', '$id_vendedor', '$total')");		  	
    		  	$SqlInserirItens->execute();
    		   endforeach;
		   echo "<script>alert('Venda Concluída')</script>";
		unset($_SESSION['carrinho']);
		}		

?>
<!DOCTYPE HTML>

<html lang="pt-BR">
	<head>
		<?php include("meta_tag.php");?>

		<title>Busca Carrinho</title>

		<?php include("css.php");?>
	
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/functions.js"></script>

	</head>

	<body>
		<?php include("header.php");?>
		<section class="buscar">
			<div class="container">

				<div class="center">
					<form action="" method="POST" enctype="multipart/form-data" id="form_busca">
						<label>
							<span>Buscar Produto</span>
							<input type="text" name="buscar" id="busca" />
						</label>
					</form>

					<div id="resultado_busca"></div>
				</div><!--center-->

			</div><!--container-->
		</section>
		<section class="tabela">
			<div class="container">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="dados_do_pedido">

						<div class="item">
							<label for="nome_vendedor">Vendedor</label>
							<input type="text" id="nome_vendedor" name="nome_vendedor" readonly value="<?php echo $vendedor_logado['nome_vendedor']?> "/>
						</div><!--item-->

						<div class="item">
							<label for="email_vendedor">E-mail</label>
							<input type="text" id="email_vendedor" name="email_vendedor" readonly value="<?php echo $vendedor_logado['e-mail_vendedor']?>"/>
						</div><!--item-->

						<div class="item">
							<label for="data_admissao">Data Admissão</label>
							<input type="text" readonly id="data_admissao" name="data_admissao" value="<?php echo  $Util->dataDecode($vendedor_logado['data_admissao']);?>"/>
						</div><!--item-->

						<div class="item">
							<label for="nome_vendedor">Cliente</label>
							<select name="id_cliente">
								<?php foreach ($cliente as $clienteLinha) { ?>
									<?php //var_dump($clienteLinha)?>;
								 <option  value="<?php echo $clienteLinha->getId_cliente();?>"><?php echo $clienteLinha->getNome_cliente();?></option>
								 <?php } ?>
							</select>	
						</div><!--item-->

					</div><!--dados_do_pedido-->

					<table border="0" cellpadding="0" cellspacing="0" width="80%">
						<thead>
							<tr>
								<td>Produto</td>
								<td>Valor</td>
								<td>Qtd</td>
								<td>Subtotal</td>
								<td>Remover</td>
								
							</tr>
						</thead>

						<tbody id="content_retorno">
							<?php
							$total = 0;
							$carrinho = (is_array($_SESSION['carrinho']) ? count($_SESSION['carrinho']) : 0);
							
							if($carrinho > 0):
								foreach($_SESSION['carrinho'] as $idProd => $qtd){
									$pegaProduto = $conexao->prepare("SELECT * FROM en_produtos WHERE id = ?");
									$pegaProduto->execute(array($idProd));
									$dadosProduto = $pegaProduto->fetchObject();
									//if($vendedor->getData_Admissao() < date("m.d.y")){
									$subTotal = ($dadosProduto->valor*$qtd);
									$total += $subTotal;
									
									echo '<tr><td>'.$dadosProduto->titulo.'</td><td>Valor</td><td><input readonly type="text" id="qtd" value="'.$qtd.'" size="3" /></td>';
									echo '<td>R$ '.number_format($subTotal, 2, ',', '.').'</td>';
									echo '<td><a class="remover "href="sys/sys.php?acao=del&id=' . $idProd . '"></a></td>';
									echo '</tr>';
								}
							
							echo '<tr><td colspan="3">Total</td><td id="total">R$ '.number_format($total, 2, ',', '.').'</td></tr>';							
							endif;							
							?>
						</tbody>
						
					</table>
					<input type="submit" name="finalizar" value="finalizar" class="botao" />
				</form>
		</div><!--container-->
	</section><!--tabela-->

	</body>
</html>