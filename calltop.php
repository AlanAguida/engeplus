<script src="../../js/jquery.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="../../imagens/elementos/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="../../css/reset.css">
<link rel="stylesheet" href="../../css/grid.css">
<link rel="stylesheet" href="../../css/menRes.css">
<link rel="stylesheet" href="../../css/estilos.css">
<link rel="stylesheet" href="../../js/fancybox/fancybox.css">
<link rel="stylesheet" href="../../js/ui/css/jquery.ui.all.css">

<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700" rel="stylesheet">

<!-- DATA TABLE -->
<link href="../../js/data_table/css/jquery.bdt.css" type="text/css" rel="stylesheet">
<link href="../../js/data_table/css/style.css" type="text/css" rel="stylesheet">
<link href="../../js/datetimepicker/datetimepicker/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="../../js/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
