-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 03/02/2021 às 23:36
-- Versão do servidor: 5.7.32-cll-lve
-- Versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `comfortm_engeplus`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_acessos`
--

CREATE TABLE `en_acessos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `datahora` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_acessos`
--

INSERT INTO `en_acessos` (`id`, `id_usuario`, `ip`, `datahora`) VALUES
(1, 1, '192.168.1.21', '2021-02-01 08:39:56'),
(2, 1, '192.168.1.21', '2021-02-01 08:45:21'),
(3, 1, '192.168.1.21', '2021-02-01 09:13:43'),
(4, 1, '192.168.1.21', '2021-02-01 11:11:51'),
(5, 1, '192.168.1.22', '2021-02-01 11:50:10'),
(6, 1, '192.168.1.7', '2021-02-01 15:14:09'),
(7, 1, '192.168.1.7', '2021-02-01 15:17:56'),
(8, 1, '192.168.1.7', '2021-02-01 15:19:11'),
(9, 1, '192.168.1.7', '2021-02-01 15:22:14'),
(10, 1, '192.168.1.7', '2021-02-01 15:22:49'),
(11, 1, '192.168.1.7', '2021-02-01 15:23:48'),
(12, 1, '192.168.1.7', '2021-02-01 15:25:20'),
(13, 1, '192.168.1.7', '2021-02-01 15:27:04'),
(14, 1, '192.168.1.7', '2021-02-01 15:27:41'),
(15, 1, '::1', '2021-02-01 19:21:58'),
(16, 1, '192.168.1.7', '2021-02-02 08:11:01'),
(17, 1, '192.168.1.7', '2021-02-02 10:41:35'),
(18, 2, '192.168.1.7', '2021-02-02 11:11:30'),
(19, 1, '192.168.1.11', '2021-02-02 13:28:05'),
(20, 1, '192.168.1.7', '2021-02-02 15:20:51'),
(21, 1, '192.168.1.7', '2021-02-02 15:24:17'),
(22, 1, '192.168.1.7', '2021-02-02 15:29:50'),
(23, 1, '192.168.1.7', '2021-02-02 15:39:01'),
(24, 1, '192.168.1.7', '2021-02-02 15:49:28'),
(25, 1, '192.168.1.7', '2021-02-02 15:50:35'),
(26, 1, '192.168.1.7', '2021-02-02 15:51:32'),
(27, 1, '192.168.1.7', '2021-02-02 15:52:12'),
(28, 1, '::1', '2021-02-02 19:48:34'),
(29, 1, '::1', '2021-02-02 20:04:13'),
(30, 1, '::1', '2021-02-02 21:33:33'),
(31, 1, '::1', '2021-02-02 21:37:20'),
(32, 1, '::1', '2021-02-02 22:12:40'),
(33, 1, '::1', '2021-02-02 22:18:19'),
(34, 2, '::1', '2021-02-02 23:05:32'),
(35, 2, '::1', '2021-02-02 23:51:55'),
(36, 1, '::1', '2021-02-03 00:35:47'),
(37, 2, '::1', '2021-02-03 03:07:03'),
(38, 1, '::1', '2021-02-03 03:19:35'),
(39, 1, '::1', '2021-02-03 18:42:48'),
(40, 1, '177.136.164.167', '2021-02-03 21:44:30'),
(41, 1, '186.209.250.215', '2021-02-03 21:48:06'),
(42, 1, '186.209.250.215', '2021-02-03 22:45:52'),
(43, 1, '186.209.250.215', '2021-02-03 22:48:03'),
(44, 1, '186.209.250.215', '2021-02-03 22:50:07'),
(45, 1, '186.209.250.215', '2021-02-03 22:51:48'),
(46, 1, '177.136.164.167', '2021-02-03 22:58:56'),
(47, 2, '186.209.250.215', '2021-02-03 23:07:30'),
(48, 1, '177.136.164.167', '2021-02-03 23:12:47'),
(49, 1, '186.209.250.215', '2021-02-03 23:27:46'),
(50, 1, '186.209.250.215', '2021-02-03 23:30:37');

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_categoria_produto`
--

CREATE TABLE `en_categoria_produto` (
  `id_categoria_prod` int(11) NOT NULL,
  `descricao_cat` varchar(50) NOT NULL,
  `percen_comissao` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_categoria_produto`
--

INSERT INTO `en_categoria_produto` (`id_categoria_prod`, `descricao_cat`, `percen_comissao`) VALUES
(1, 'Produto', 0.1),
(2, 'Serviço', 0.25);

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_cliente`
--

CREATE TABLE `en_cliente` (
  `id_cliente` bigint(11) NOT NULL,
  `nome_cliente` varchar(120) NOT NULL,
  `e-mail_cliente` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_cliente`
--

INSERT INTO `en_cliente` (`id_cliente`, `nome_cliente`, `e-mail_cliente`) VALUES
(1, 'alan Richard de Águida', 'alanrichardaguida@hotmail.com'),
(2, 'Engeplus Data Center', 'engeplus@engeplus.com.br');

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_item_pedido`
--

CREATE TABLE `en_item_pedido` (
  `item_id` bigint(20) NOT NULL,
  `prod_id` bigint(20) NOT NULL,
  `ped_id` bigint(20) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `valor_item` double NOT NULL,
  `comissao_item` double NOT NULL,
  `id_vendedor` bigint(20) NOT NULL,
  `total_pedido` double NOT NULL,
  `id_cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_item_pedido`
--

INSERT INTO `en_item_pedido` (`item_id`, `prod_id`, `ped_id`, `quantidade`, `valor_item`, `comissao_item`, `id_vendedor`, `total_pedido`, `id_cliente`) VALUES
(1, 1, 1, 1, 1200.37, 120.04, 2, 4450.64, NULL),
(2, 2, 1, 1, 3000.27, 300.03, 2, 4450.64, NULL),
(3, 3, 1, 1, 250, 62.5, 2, 4450.64, NULL),
(4, 1, 2, 1, 1200.37, 360.11, 1, 4450.64, NULL),
(5, 2, 2, 1, 3000.27, 900.08, 1, 4450.64, NULL),
(6, 3, 2, 1, 250, 75, 1, 4450.64, NULL),
(7, 1, 3, 1, 1200.37, 360.11, 1, 4200.64, NULL),
(8, 2, 3, 1, 3000.27, 900.08, 1, 4200.64, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_login`
--

CREATE TABLE `en_login` (
  `id` int(11) NOT NULL,
  `senha` varchar(600) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_login`
--

INSERT INTO `en_login` (`id`, `senha`, `nome`) VALUES
(1, '2df11c88b2888a629fb99594b1ff62c8ecf7a0db861812dbc4fc0d4bd1935ac1ef0930bb54c18ac6444497b4ecca0d533e6b30657262506706b86bd2fe136ff4', 'engeplus'),
(2, '2df11c88b2888a629fb99594b1ff62c8ecf7a0db861812dbc4fc0d4bd1935ac1ef0930bb54c18ac6444497b4ecca0d533e6b30657262506706b86bd2fe136ff4', 'engeplus2');

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_pedido`
--

CREATE TABLE `en_pedido` (
  `id_pedido` bigint(20) NOT NULL,
  `id_cliente` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_pedido`
--

INSERT INTO `en_pedido` (`id_pedido`, `id_cliente`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_produtos`
--

CREATE TABLE `en_produtos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `valor` double(10,2) NOT NULL,
  `cat_prod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_produtos`
--

INSERT INTO `en_produtos` (`id`, `titulo`, `valor`, `cat_prod_id`) VALUES
(1, 'Hospedagem Linux Apache e MySql', 1200.37, 1),
(2, 'Hospedagem Windows IIS e MsSQL Server ', 3000.27, 1),
(3, 'Instalação Fibra ótica até 30 m do poste', 250.00, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `en_vendedor`
--

CREATE TABLE `en_vendedor` (
  `id_vendedor` int(11) NOT NULL,
  `nome_vendedor` varchar(120) NOT NULL,
  `e-mail_vendedor` varchar(100) NOT NULL,
  `data_admissao` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `en_vendedor`
--

INSERT INTO `en_vendedor` (`id_vendedor`, `nome_vendedor`, `e-mail_vendedor`, `data_admissao`) VALUES
(1, 'Engeplus', 'engeplus@engeplus.com.br', '2015-04-01'),
(2, 'Engeplus2', 'engeplus2@engeplus.com.br', '2020-06-01');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `valor` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `produtos`
--

INSERT INTO `produtos` (`id`, `titulo`, `valor`) VALUES
(1, 'Curso de loja virtual com PHP 2.0', '120.40'),
(2, 'Curso de programação orientada a objetos', '95.40'),
(3, 'Curso de PHP básico', '85.70');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `en_acessos`
--
ALTER TABLE `en_acessos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `en_categoria_produto`
--
ALTER TABLE `en_categoria_produto`
  ADD PRIMARY KEY (`id_categoria_prod`);

--
-- Índices de tabela `en_cliente`
--
ALTER TABLE `en_cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Índices de tabela `en_item_pedido`
--
ALTER TABLE `en_item_pedido`
  ADD PRIMARY KEY (`item_id`);

--
-- Índices de tabela `en_login`
--
ALTER TABLE `en_login`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `en_pedido`
--
ALTER TABLE `en_pedido`
  ADD PRIMARY KEY (`id_pedido`);

--
-- Índices de tabela `en_produtos`
--
ALTER TABLE `en_produtos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `en_vendedor`
--
ALTER TABLE `en_vendedor`
  ADD PRIMARY KEY (`id_vendedor`);

--
-- Índices de tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `en_acessos`
--
ALTER TABLE `en_acessos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de tabela `en_categoria_produto`
--
ALTER TABLE `en_categoria_produto`
  MODIFY `id_categoria_prod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `en_cliente`
--
ALTER TABLE `en_cliente`
  MODIFY `id_cliente` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `en_item_pedido`
--
ALTER TABLE `en_item_pedido`
  MODIFY `item_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `en_login`
--
ALTER TABLE `en_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `en_pedido`
--
ALTER TABLE `en_pedido`
  MODIFY `id_pedido` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `en_produtos`
--
ALTER TABLE `en_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `en_vendedor`
--
ALTER TABLE `en_vendedor`
  MODIFY `id_vendedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
