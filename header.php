<?php
require_once "autoload.php";
	$Usuario = new Usuario();
	$UsuarioDAO = new UsuarioDAO();

	$acessos = $UsuarioDAO->buscaAcessos($_SESSION['usuarioIdAdmin']);

	setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');
	//var_dump($acessos);
	$date = date_create($acessos['datahora']);
	$dataAcesso = date_format($date, 'd/m/Y H:i:s');

	$NomeUser = $_SESSION['usuarioNomeAdmin'];


	$usuarioDAO = new UsuarioDAO();

	if(!$usuarioDAO->usuarioEstaLogado()){
		$usuarioDAO->deslogaUsuario();
		exit;

	}
	?>

	<header>
			<div class="container">

				<div class="logo">
					<img src="img/logo.png">
				</div><!--logo-->				

				<div class="info">
					
					<div tabindex="0" class="menu">
					    <div class="menu-dropdown">
					    	<a href="index.php">Cadastrar</a>
							<a href="pedidos.php">Pedidos</a>
							<a href="model/sair.php">Sair</a>
					    </div>
					</div><!--menu-->

					<h5>Bem-vindo</h5>

					<span>
						<p>
						<b><?php echo $NomeUser?></b>
						<br>

						Último Acesso:

						<?php echo $dataAcesso?>
						</p>
					</span>

				</div><!--info-->		
			</div><!-- grid-r -->

			</div><!--container-->
		</header>