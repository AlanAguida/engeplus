<?php 
session_start();

?>
<!DOCTYPE html>
<html lang="br">
    <head>
        <?php include("meta_tag.php");?>

        <title>Engeplus</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/style.css">

        <script src="js/modernizr.js"></script>

    </head>

    <body>
        

        <div class="fundo_login">
            <div class="container">

                <div class="logo">
                    <img src="img/logo.png" alt="Logo">
                </div><!--logo-->

                <div class="item_login">

                    <div class="content_login">


                        <form action="valida_login.php" method="POST" accept-charset="utf-8">
                            <div class="padding">
                                <div class="margin">
                                    <h5>USUÁRIO</h5>
                                    <input type="text" name="usuario" placeholder="" required="">
                                </div><!--margin-->

                                <div class="margin">
                                    <h5>SENHA</h5>
                                    <input type="password" name="senha" placeholder="" required="">
                                </div><!--margin-->

                                <div class="clearfix">
                                    <input class="btn" type="submit" name="" value="Entrar">
                                </div><!--clearfix-->
                            </div><!--padding-->                           
                        </form>

                       
                    </div><!--content_login-->

                <?php if(!empty($_SESSION['danger'])){ ?>
                    <label class="g6div aviso ativo">
                        <div class="grid-r">
                            <div class="g4div" data-celular="g12div" data-tablet="g12div">
                                <img src="../../imagens/elementos/ico_x.png">
                            </div>
                            <div class="g8div larguraTotalComCondicao" data-celular="g12div" data-tablet="g12div">
                                <span>Usuário ou senha incorretos. <br /> Verifique com o administrador.</span>
                            </div>
                        </div>
                    </label>
                 <?php } ?>

                </div><!--item_login-->

            </div><!--container-->
        </div><!--fundo_login-->

        <?php include("js.php");?>

    </body>

</html>






