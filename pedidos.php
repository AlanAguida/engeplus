<?php
	session_start();
	require_once "autoload.php";
	$Usuario = new Usuario();
	$UsuarioDAO = new UsuarioDAO();
	
	$acessos = $UsuarioDAO->buscaAcessos($_SESSION['usuarioIdAdmin']);
	setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');
	$date = date_create($acessos['datahora']);
	$dataAcesso = date_format($date, 'd/m/Y H:i:s');
	$NomeUser = $_SESSION['usuarioNomeAdmin'];
	$id_vendedor = $_SESSION['usuarioIdAdmin'];

	$usuarioDAO = new UsuarioDAO();

	if(!$usuarioDAO->usuarioEstaLogado()){
		$usuarioDAO->deslogaUsuario();
		exit;
	}

	//include_once "DB.php";
	$conexao = DB::conexao();	
	include_once "model/VendedorDAO.php";
	include_once "model/Vendedor.php";
	include_once "model/Mobject.php";
	include_once "model/Util.php";
	include_once "model/Cliente.php";
	include_once "model/ClienteDAO.php";
	$Util =  new Util();
	$clientes = new ClienteDAO();
	$cliente = new Cliente();
	$vendedor = new Vendedor();
	$vendedorDAO = new VendedorDAO();
	$pedidos = new PedidoDAO();
	$pedidos = $pedidos->listaPedidos();
	
	$cliente = $clientes->listaClientes();
	
	$vendedor = $vendedorDAO->buscaVendedor($id_vendedor);
	$vendedor_logado = $vendedorDAO->buscaVendedor($id_vendedor);

?>
<!DOCTYPE HTML>

<html lang="pt-BR">
	<head>
		<?php include("meta_tag.php");?>
		<title>Busca Carrinho</title>
		<?php include("css.php");?>	
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/functions.js"></script>
	</head>

	<body>
		<?php include("header.php");?>
		<section class="tabela">
			<div class="container">		
				<table border="0" cellpadding="0" cellspacing="0" width="80%">
					<thead>
						<tr>
							<td>Pedido #</td>
							<td>Cliente</td>
							<td>Vendedor</td>
							<td>Comissão vendedor</td>							
						</tr>
					</thead>
					<tbody>
						<?php foreach ($pedidos as $key => $pedidoAtual) { ?>
							<tr>
								<td><?php echo $pedidoAtual->getId_pedido()?></td>					
								<td><?php echo $pedidoAtual->getNome_cliente()?></td>
								<td><?php echo $pedidoAtual->getNome_vendedor()?></td>
								<td><?php echo '<span>'.'R$ '.number_format($pedidoAtual->getComissao(),2,'.',',')?></td>
							</tr>
							
						<?php } ?>
							<tr>
							<td></td>
							<td> </td>
						</tr>
					</tbody>
				</table>
			</div><!--container-->
		</section><!--tabela-->
	</body>
</html>